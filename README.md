# Welcome

The FileParser is a stream and IEnumerable<string> based reader of delimited and fixed width files. It can read most any text stream of any length. It uses a design that does not increase memory consumption as the stream is read.

##Supports
*Delimited text - the delimiter can only be on character
*Supports quoted strings - the character for qualifying quoted text is configurable. The configured delimiter is allowed to be in the quoted text.
*The number of columns and rows the parser can deal with is only limited by the computers system resources.

### Contribution guidelines ###

* Unit tests are required for each contribution
